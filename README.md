# Mutagen
Game Master asset generation tool for Mutant Year Zero.

# How to use
* start mongodb
* compile and start java spring boot application
* start angular with ng serve within frontend/mutagen
* go to localhost:4200 and you're good to go!

# ui-icons
Choose from these! -->
https://icon-icons.com/pack/Arrows/4012
