import { Component, OnInit } from '@angular/core';
import { MutagenService, Zone } from './mutagen.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  zone: Zone | undefined;

  constructor(
    private mutagenService: MutagenService) {
  }

  ngOnInit() {
    this.mutagenService.getZone(17).subscribe((value) => this.zone = value);
  }

  createZone() {
    this.mutagenService.createZone("Test Zone").subscribe(zone => {
      this.zone = zone;
    });
  }

  getZone(id: number) {
    this.mutagenService.getZone(id).subscribe((value) => this.zone = value);
  }
}
