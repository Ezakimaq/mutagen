import { Component, Input } from '@angular/core';
import { Sector, Zone } from '../mutagen.service';

@Component({
  selector: 'mutagen-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})

export class MapComponent {
  @Input() zone: Zone | undefined;

  constructor() {
  }

  showSector(sector : Sector) {
     alert('Environment: ' + sector.environment + '\n' +
       'Mood: ' + sector.mood + '\n' +
       'Ruin: ' + sector.ruin + '\n' +
       'Artifacts: ' + sector.artifacts + '\n' +
       'Scraps: ' + sector.scraps
     );
  }
}
