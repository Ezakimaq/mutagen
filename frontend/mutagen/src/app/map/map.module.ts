import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { MapComponent } from './map.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    MapComponent
  ],
  imports: [
    BrowserModule,
    NgbModule,
  ],
  providers: [],
  exports: [
    MapComponent
  ],
  bootstrap: [MapComponent]
})
export class MapModule { }
