import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class MutagenService {
  private apiUrl = 'http://localhost:8080/api/v1';

  constructor(private http: HttpClient) {
  }

  createZone(name: string): Observable<Zone> {
    return this.http.post<Zone>(`${this.apiUrl}/zone/create`, name);
  }

  getZone(id: number): Observable<Zone> {
    return this.http.get<Zone>(`${this.apiUrl}/zone?id=` + id);
  }
  //
  // getSector(id: number) {
  //   //return this.zone.sectorData.findIndex()
  // }
}

export interface Zone {
  id: number;
  name: string;
  sectors: Sector[];
}

export interface Sector {
  environment: string;
  mood: string;
  ruin: string,
  artifacts: string[],
  scraps: string[]
}

