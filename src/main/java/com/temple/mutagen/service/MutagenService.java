package com.temple.mutagen.service;

import static java.util.stream.Stream.generate;

import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.springframework.stereotype.Service;

import com.temple.mutagen.model.Sector;
import com.temple.mutagen.model.Zone;
import com.temple.mutagen.repository.AssetRepository;

@Service
public class MutagenService {
	private static final int NumberOfSectors = 600;

	private final AssetRepository assetRepository;
	private final AssetCache assetCache;

	public MutagenService(AssetRepository assetRepository, AssetCache assetCache) {
		this.assetRepository = assetRepository;
		this.assetCache = assetCache;
	}

	public Optional<Zone> createZone(String name) {
		var zone = new Zone(getZoneId(), name, createSectors());
		System.out.printf("creating zone with id: " + zone.id());
		return Optional.of(assetRepository.save(zone));
	}

	public Optional<Zone> getZone(long id) {
		System.out.println("get zone with id: " + id);
		return Optional.of(assetRepository.findZoneById(id));
	}

	public Sector[] createSectors() {
		return generate(this::createSector)
				.limit(NumberOfSectors)
				.toArray(Sector[]::new);
	}

	private Sector createSector() {
		Random random = new Random();
		return new Sector(
				assetCache.getEnvironments().get((random.nextInt(assetCache.getEnvironments().size()))),
				assetCache.getMoods().get(random.nextInt(assetCache.getMoods().size())),
				assetCache.getRuins().get(random.nextInt(assetCache.getRuins().size())),
				List.of(assetCache.getArtifacts().get(random.nextInt(assetCache.getArtifacts().size())),
						assetCache.getArtifacts().get(random.nextInt(assetCache.getArtifacts().size()))),
				List.of(assetCache.getScraps().get(random.nextInt(assetCache.getScraps().size())),
						assetCache.getScraps().get(random.nextInt(assetCache.getScraps().size())),
						assetCache.getScraps().get(random.nextInt(assetCache.getScraps().size()))));
	}

	private long getZoneId() {
		return assetRepository.count() + 1;
	}

}
