package com.temple.mutagen.service;

import static com.temple.mutagen.model.AssetType.ARTIFACT;
import static com.temple.mutagen.model.AssetType.ENVIRONMENT;
import static com.temple.mutagen.model.AssetType.HUMANOID;
import static com.temple.mutagen.model.AssetType.MONSTER;
import static com.temple.mutagen.model.AssetType.MOOD;
import static com.temple.mutagen.model.AssetType.PHENOMENON;
import static com.temple.mutagen.model.AssetType.RUIN;
import static com.temple.mutagen.model.AssetType.SCRAP;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.temple.mutagen.model.AssetType;
import com.temple.mutagen.model.Humanoid;
import com.temple.mutagen.model.Monster;
import com.temple.mutagen.model.Phenomenon;

@Component
public class AssetCache {
	private List<String> scraps;
	private List<String> artifacts;
	private List<String> environments;
	private List<String> moods;
	private List<String> ruins;
	private List<Phenomenon> phenomenons;
	private List<Monster> monsters;
	private List<Humanoid> humanoids;

	public AssetCache() {
		Arrays.stream(AssetType.values())
				.toList()
				.forEach(this::populate);
	}

	private void populate(AssetType assetType) {
		try (BufferedReader reader = new BufferedReader(new FileReader("src/main/resources/" + assetType.toString() + ".txt"))) {
			switch (assetType) {
				case ARTIFACT -> {
					artifacts = reader.lines().toList();
				}
				case MOOD -> {
					moods = reader.lines().toList();
				}
				case ENVIRONMENT -> {
					environments = reader.lines().toList();
				}
				case SCRAP -> {
					scraps = reader.lines().toList();
				}
				case RUIN -> {
					ruins = reader.lines().toList();
				}
				case MONSTER -> {
					monsters = populateMonsters(reader);
				}
				case HUMANOID -> {
					humanoids = populateHumanoids(reader);
				}
				case PHENOMENON -> {
					phenomenons = populatePhenomenons(reader);
				}
			}
		} catch (IOException e) {
			System.out.println(" error: " + e);
		}
	}

	private static List<Monster> populateMonsters(BufferedReader reader) {
		return reader.lines().map(line -> {
			String[] parts = line.split(",");
			if (parts.length == 6) {
				return new Monster(parts[0], parts[1], parts[2], parts[3], parts[4], parts[5]);
			} else {
				throw new IllegalArgumentException("Invalid CSV line: " + line);
			}
		}).collect(Collectors.toList());
	}

	private static List<Humanoid> populateHumanoids(BufferedReader reader) {
		return reader.lines().map(line -> {
			String[] parts = line.split(",");
			if (parts.length == 6) {
				return new Humanoid(parts[0], parts[1], parts[2], parts[3], parts[4], parts[5]);
			} else {
				throw new IllegalArgumentException("Invalid CSV line: " + line);
			}
		}).collect(Collectors.toList());
	}

	private static List<Phenomenon> populatePhenomenons(BufferedReader reader) {
		return reader.lines().map(line -> {
			String[] parts = line.split(",");
			if (parts.length == 3) {
				return new Phenomenon(parts[0], parts[1], parts[2]);
			} else {
				throw new IllegalArgumentException("Invalid CSV line: " + line);
			}
		}).collect(Collectors.toList());
	}

	public List<String> getScraps() {
		return scraps;
	}
	public List<String> getArtifacts() {
		return artifacts;
	}
	public List<String> getEnvironments() {
		return environments;
	}
	public List<String> getMoods() {return moods;}
	public List<String> getRuins() {return ruins;}
	public List<Phenomenon> getPhenomenons() {
		return phenomenons;
	}
	public List<Monster> getMonsters() {
		return monsters;
	}
	public List<Humanoid> getHumanoids() {
		return humanoids;
	}
}
