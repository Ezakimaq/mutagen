package com.temple.mutagen.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.temple.mutagen.model.Zone;

@Repository
public interface AssetRepository extends MongoRepository<Zone, String> {
	Zone findZoneById(long id);
}