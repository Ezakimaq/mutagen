package com.temple.mutagen.controller;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.temple.mutagen.model.Zone;
import com.temple.mutagen.service.MutagenService;

@Controller
@RequestMapping(value = "/api/v1")
public class MutagenController {
	private final MutagenService mutagenService;

	public MutagenController(MutagenService mutagenService) {
		this.mutagenService = mutagenService;
	}

	//http://localhost:8080/api/v1/zone/create?name=<String>
	//curl -X POST -d name=test localhost:8080/api/v1/zone/create
	@CrossOrigin()
	@RequestMapping(value = "/zone/create", method = POST)
	public ResponseEntity<Zone> createZone(@RequestBody String name) {
		return ResponseEntity.of(mutagenService.createZone(name));
	}

	//http://localhost:8080/api/v1/zone?id=<number>
	@CrossOrigin()
	@RequestMapping(value = "/zone", method = GET)
	public ResponseEntity<Zone> getZone(@RequestParam(value = "id") @NonNull long id) {
		return ResponseEntity.of(mutagenService.getZone(id));
	}
}