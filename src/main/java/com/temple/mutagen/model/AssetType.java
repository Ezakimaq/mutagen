package com.temple.mutagen.model;

public enum AssetType {
	ARTIFACT,
	MOOD,
	ENVIRONMENT,
	MONSTER,
	HUMANOID,
	PHENOMENON,
	SCRAP,
	RUIN
}
