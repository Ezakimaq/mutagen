package com.temple.mutagen.model;

public record Humanoid(String name,
                       String description,
                       String attributes,
                       String skills,
                       String mutations,
                       String weapons){
}
