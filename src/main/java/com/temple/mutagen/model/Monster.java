package com.temple.mutagen.model;

public record Monster(String name,
                      String description,
                      String attributes,
                      String skills,
                      String armor,
                      String weapons
                      ) {
}
