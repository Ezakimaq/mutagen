package com.temple.mutagen.model;

public record Threat(ThreatType threatType, int threatLevel, String description, int wits, int strength, int agility, int empathy) {
}
