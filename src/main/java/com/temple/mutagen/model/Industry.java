package com.temple.mutagen.model;

import org.apache.catalina.Pipeline;

public enum Industry{
    Factory,
    MilitaryBase,
    OilCistern,
    Pipeline,
    PurificationPlant,
}
