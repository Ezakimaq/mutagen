package com.temple.mutagen.model;

public enum ThreatType {
	HUMANOID,
	MONSTER,
	PHENOMENON
}
