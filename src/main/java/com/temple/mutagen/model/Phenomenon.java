package com.temple.mutagen.model;

public record Phenomenon(String name, String description, String effect) {
}
