package com.temple.mutagen.model;

public record Zone(long id, String name, Sector[] sectors) {
}
