package com.temple.mutagen.model;

import java.util.List;

public record Sector(String environment, String mood, String ruin, List<String> artifacts, List<String> scraps) {
}